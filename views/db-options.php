<?php
class db_gogo
{
	function init()
	{
		global $jal_db_version;
		$jal_db_version = "1.0";

		function jal_install () 
		{
			global $wpdb;
			$table_name = $wpdb->prefix . "blankplugin";
			if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) 
			{
				$sql = "CREATE TABLE " . $table_name . " (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				tiny_text longtext NOT NULL,
				UNIQUE KEY id (id)
				);";

				require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
				dbDelta($sql);
				$tiny_text = "lorem text";

				$rows_affected = $wpdb->insert
				( $table_name, array
					( 
						'tiny_text' => $tiny_text
						) 
					);

				add_option("jal_db_version", $jal_db_version);
			}
		}
	}
}
