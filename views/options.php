<?php
class options_gogo
{
	function init()
	{
		add_action('admin_menu', 'p_menu');
		add_action( 'admin_init', 'p_init' );

		function p_init() 
		{
			wp_register_style( 'trueplugin', plugins_url('css/style.css', __FILE__) );
		}

		function p_menu() 
		{
			$page_suffix = add_menu_page
			(
				'BlankPlugin Settings', //title
				'BlankPlugin', //menu title
				'administrator', //capability
				'blankplugin', //menu slug
				'pl_settings_page', //function
				'', //icon
				null //position
				);

			add_action( 'admin_init', 'reg_opt' );
			add_action( 'admin_print_styles-' . $page_suffix, 'styles_init' );
		}

		function reg_opt() 
		{
			register_setting( 'pl-settings-group', 'plBL_text_box' );
			register_setting( 'pl-settings-group', 'plBL_check_box' );
			register_setting( 'pl-settings-group', 'plBL_textarea_editor' );
		}

		function pl_settings_page() 
		{

			$wp_edit = array(
				'mode'              => 'textareas',
				'theme'             => 'advanced'
				);

			global $wpdb;
			$table_name = $wpdb->prefix . "blankplugin";

			//get table data
			$res = $wpdb->get_col( "SELECT tiny_text FROM " . $table_name . " WHERE id = 1" );

			//save table data
			// if($_POST['submit']) 
			// {
			// 	$wpdb->query( $wpdb->prepare( "INSERT INTO " . $table_name . " (tiny_text) VALUES 'lorem' WHERE id = 1") );
			// }
			?>

			<div class="bp-wrap">
				<h2>BlankPlugin</h2>
				<form method="post" action="options.php">
					<?php settings_fields( 'pl-settings-group' ); ?>
					<div class="bp-wrap--bp-settings">
						<label>
							<span>Text Box:</span>
							<input type="text" name="plBL_text_box" value="<?php echo get_option('plBL_text_box'); ?>" />
						</label>
						<br>
						<br>
						<label>
							<span>Clean head:</span>
							<input type="checkbox" name="plBL_check_box" value="1" <?php checked( '1', get_option( 'plBL_check_box' ) ); ?>/>
						</label>
						<br>
						<br>
						<span>Textarea from get_option:</span>
						<div class="bp-settings--editor-cover">
							<?php wp_editor(( __(get_option('plBL_textarea_editor', ''))), 'plBL_textarea_editor', $wp_edit); ?>
						</div>
						<br>
						<br>
						<span>Textarea from database:</span>
						<br>
						<textarea name="tad" id="tad"><?php echo $res[0]?></textarea>
					</div>
					<div class="submit">
						<input name="submit" type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
					</div>
				</form>
			</div>
			<?php 
		}

		function styles_init()
		{
			wp_enqueue_style( 'trueplugin' );
		}
	}

	function runWPfunctions()
	{
		$plBL_text_box = get_option('plBL_text_box');
		$checked = get_option('plBL_check_box');

		if ($checked) {
			remove_action('wp_head', 'wp_generator');
		}
	}

} // .options_gogo