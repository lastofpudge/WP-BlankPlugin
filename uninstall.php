<?php
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();

delete_option( 'plBL_text_box' );
delete_option( 'plBL_check_box' );
delete_option( 'plBL_textarea_editor' );
?>