<?php
/*
Plugin Name: BlankPlugin
Plugin URI: http://www
Description: This is a BlankPlugin
Version: 0.0.5
Author: Andrey D.
Author URI: http://www
*/

/*
Copyright (C) 2016 Andrey D., iluxor@yandex.ru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @package BlankPlugin
 * @version 0.0.5
 */

// Secure
if ( ! defined( 'ABSPATH' ) ) return;
define( 'BPP', plugin_dir_path( __FILE__ ) );

require_once( BPP .'/views/options.php' );
require_once( BPP .'/views/db-options.php' );

$options_gogo = new options_gogo();
$options_gogo->init();
$options_gogo->runWPfunctions();

$db_gogo = new db_gogo();
$db_gogo->init();
register_activation_hook(__FILE__,'jal_install');
?>